﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaPruebaDapper.Model
{
    public class PersonaDto
    {
        public string nombre;
        public string email;
        public string calle;
    }
}
