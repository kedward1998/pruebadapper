﻿using ConsolaPruebaDapper.Model;
using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaPruebaDapper
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var connectionString = "Data Source=.;Initial Catalog=PruebaDapper;Integrated Security=True";

            var query = "select p.Nombre, p.Email, d.Calle from Direcciones d, Personas p where d.PersonaId = p.Id and p.Id = @Id and p.Nombre = @Nombre";

            //DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@Id", InvoiceKind, DbType.Int32, 1);

            using (var connection = new SqlConnection(connectionString))
            {
                //var result = connection.Query(query).ToList();
                //var result  = connection.Execute(query, parameter);

                List<PersonaDto> lpersona = new List<PersonaDto>();

                lpersona = connection.Query<PersonaDto>(query, new { Id = 1, Nombre = "Felipe" }).ToList();

            }


        }
    }
}
